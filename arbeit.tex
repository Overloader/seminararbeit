\documentclass[oneside, 11pt, notitlepage, a4paper, numbers=noenddot]{scrartcl}

\usepackage{thisemi}
\usepackage[style=numeric]{biblatex}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{lipsum}
\usepackage{xcolor}

\definecolor{codegray}{rgb}{0.9, 0.9, 0.9}
\definecolor{codepurple}{rgb}{0.58, 0, 0.82}
\definecolor{codegreen}{rgb}{0, 0.6, 0}
\definecolor{codeblue}{rgb}{0, 0, 0.8}

\lstdefinestyle{mystyle}{
	backgroundcolor=\color{codegray},
	commentstyle=\color{codegreen},
	keywordstyle=\color{codeblue},
	numberstyle=\tiny\color{codepurple},
	stringstyle=\color{codepurple},
	basicstyle=\ttfamily\footnotesize,
	breakatwhitespace=false,
	breaklines=true,
	keepspaces=true,
	numbersep=5pt,
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	tabsize=2
}

\lstset{style=mystyle}

\renewcommand{\lstlistingname}{Beispiel}

\graphicspath{ {./images/} }

\addbibresource{quellen.bib}

\title {ROS2: Framework für Roboter-Software}
\author{Lukas Voreck}
\date  {2023-11-21}

\begin{document}

\begin{samepage}

	\maketitle

	\section{Einleitung}

	Die Forschung im Bereich des autonomen Fahrens hat in den vergangenen Jahren
	zu einer Fülle von innovativen Technologien geführt.
	Diese Fortschritte beschränken sich nicht nur darauf, neue Ansätze zu entwickeln, sondern beinhalten
	auch die Integration bewährter Technologien aus verwandten Bereichen wie der Robotik.
	So wurde schon von BMW daran geforscht, das Robotik-Framework ROS zu integrieren.~\cite{BMW}
	Die Entscheidung, Technologien aus der Robotik anzuwenden, erweist sich als schlüssig,
	wenn man die grundlegende Natur autonomer Fahrzeuge genauer betrachtet.

	Autonome Fahrzeuge lassen sich im Wesentlichen als mobile Roboter betrachten,
	ausgestattet mit einer Vielzahl von Sensoren, die Informationen aus ihrer Umgebung sammeln.
	Diese Daten werden dann verarbeitet, um intelligente Entscheidungen zu treffen,
	die das Fahrzeug sicher durch den Verkehr steuern sollen.
	Angesichts dieser Ähnlichkeiten liegt es nahe, etablierte Robotik-Frameworks zu nutzen.

	Ein solches Framework ist ROS 2. Auf diesem, speziell der Verwendung für die Entwickler,
	soll der Fokus dieser Arbeit liegen.

	\section{Was ist ROS?}

	ROS 2 ist ein in der Robotik weit verbreitetes
	und als ein Standard anerkanntes Framework.~\cite{Concise}
	Dies ist eine Weiterentwicklung des Robot Operating System oder kurz: ROS,
	die Echtzeit Applikationen und modernere Technologien,
	wie etwa Schwarm-Roboter, unterstützen soll.~\cite{Why}
	Es handelt sich um Framework, das nicht nur Bibliotheken und Werkzeuge
	für die Entwicklung von Robotern bereitstellt,
	sondern auch eine spezifische Entwicklungs-Methodik voraussetzt.~\cite{Concise}
	Eine sogenannte Middleware.~\cite{Concise}
	Als Open-Source-Projekt mit einer breiten Nutzerbasis~\cite{Concise}
	bietet ROS 2 zahlreiche integrierbare Bibliotheken, die von dieser erstellt wurden.~\cite{Concise}
	Zudem stellen viele Hardwarekomponenten Treiber bereit,
	die nahtlos in das Framework integriert werden können.\cite{Concise}

	Ursprünglich entstand ROS an der Stanford University,
	an der Eric Berger und Keenan \mbox{Wyrobek} anfingen, an einem Framework zu arbeiten,
	das die Arbeit mit Robotern zu vereinfachen soll.~\cite{Wizards}
	Die Entwicklung wurde im Research lab Willow Garage fortgesetzt,~\cite{Wizards}
	welches auch die Open Source Robotics Foundation,
	später zu Open Robotics umbenannt,~\cite{Wikipedia} gründete.~\cite{OSRFAnnounce}
	Diese übernahm die Weiterentwicklung von ROS.~\cite{Wikipedia}
	Open Robotics kündigte schließlich ROS 2 mit dem Ziel der Modernisierung an.~\cite{Why}
	Die erste Version von ROS 2 wurde im Dezember 2017 veröffentlicht.\cite{Concise}

	Im Verlauf dieser Arbeit bezieht sich die Verwendung des Begriffs ''ROS'' auf ROS 2,
	um die Lesbarkeit zu erleichtern.

	\section{Design}

	Um die Funktionsweisen von ROS zu verstehen ist es hilfreich,
	die verschiedenen Schichten des Frameworks zu betrachten,
	wie in Abbildung \ref{fig:layers} dargestellt.

	\begin{figure}[h]
		\centering
		\includegraphics[width=0.75\textwidth]{Layers}
		\caption{\label{fig:layers}Schichten des Frameworks \cite{Concise}}
	\end{figure}

	Die oberste Schicht, der User Code, bezeichnet den individuell programmierten Code,
	der das Verhalten des Roboters steuert. Diese Schicht muss vermutlich nicht weiter erläutert werden.

	Die unterste Schicht, die OS Schicht, beinhaltet das Betriebssystem,
	auf dem die ROS-Applikation läuft oder entwickelt wird.

	Die RMW-Ebene ist für die Kommunikation innerhalb einer ROS-Applikation verantwortlich.
	Hier kümmert sich ein Data Distribution Service (DDS) um eine dezentrale,
	auf Publication/Subscription basierende Kommunikation~\cite{Concise}
	mit Echtzeitcharakteristika.~\cite{Concise}
	Diese Art der Kommunikation ist notwendig, da eine ROS-Applikation ein verteiltes System ist,
	das nicht nur in einem einzelnen Prozess,
	sondern teilweise nicht einmal auf einem einzelnen Gerät läuft~\cite{Concise}.
	DDS ist ein standard, für den es mehrere Implementierungen gibt,
	von denen drei in der Grafik zu sehen sind.~\cite{Concise}
	Obwohl die meisten Nutzer zwischen diesen keine Leistungsunterschiede feststellen werden,
	ist es bei komplexeren Applikationen möglich, durch die Berücksichtigung unterschiedlicher
	Kriterien eine spezifische Implementierung auszuwählen~\cite{Concise}.

	Die letzte verbleibende Ebene, das ROS Client Layer, ist die Ebene, mit der der Entwickler interagiert.
	Es gibt zwei offiziell unterstützte Sprachen zur Entwicklung einer ROS-Applikation,
	deren Bibliotheken in dieser Ebene liegen:
	\lstinline{rclcpp} für C++ und \lstinline{rclpy} für Python~\cite{Concise}.
	Neben den offiziell unterstützten gibt es weitere Bibliotheken aus der Community für Sprachen wie
	Java, Rust, NodeJS und mehr~\cite{Concepts}.
	Um sicherzustellen, dass alle Bibliotheken das gleiche Verhalten aufweisen und somit konsistent sind,
	liegt diesen eine weitere Bibliothek zugrunde, auf der alle anderen basieren:
	die C-Implementierung der ROS-Funktionalitäten, \lstinline{rclc}\cite{Concise}.
	Diese implementiert die Funktionalitäten, die nicht sprachspezifisch sind\cite{Concepts}.
	Bei Änderungen am Grundverhalten von ROS müssen somit nicht alle Bibliotheken,
	sondern nur \lstinline{rclc} aktualisiert werden~\cite{Concepts}.
	Sprachspezifische Konzepte, wie Threading-Modelle, die bestimmen,
	wie der Code ausgeführt wird, werden von den sprachspezifischen Bibliotheken implementiert~\cite{Concepts}.

	Der Großteil der Beispiele in dieser Arbeit ist in C++ angegeben
	und nutzt daher die \lstinline{rclcpp}-Bibliothek.
	Allerdings werden in den Beispielen etwas verkürzt dargestellt,
	so werden etwa die Header Dateien nicht angegeben.

	\section{Computation Graph}

	Jede ROS Applikation ist ein Computation Graph.~\cite{Concise}
	Die Applikation ist Organisiert in einzelne Nodes, auf denen der Code läuft,~\cite{Concise, Concepts}
	und die über unterschiedliche Paradigmen miteinander kommunizieren.~\cite{Concise, Concepts}
	Zu diesen Nodes gehört nicht nur der Code, der den Roboter steuern soll,
	sondern auch Monitoring Tools,~\cite{Concise}
	die zum Beispiel die Kommunikation zwischen Nodes überwachen.
	Standardmäßig sollte jede Node nur eine Aufgabe erfüllen~\cite{Concepts}
	und in einem eigenen Prozess laufen.~\cite{Concise}

	ROS nutzt intensiv objektorientierte Programmierung.~\cite{Concise}
	Dies zeigt sich unter anderem darin,
	dass Nodes eigene Objekte sind,~\cite{Concise}
	die aus einer Node Klasse instantiiert werden können,
	zu sehen in Beispiel \ref{lst:Instantiierung}.
	\begin{lstlisting}[language=c++, caption=Instantiierung einer Node \cite{Concise}, label={lst:Instantiierung}]
int main (int argc, char *argv[]) {
    // Initialisiere rclcpp
    rclcpp::init(argc, argv);

    // Erzeuge eine Node
    auto node = rclcpp::Node::make_shared("simple_node");
    // Führe die Funktionen der Node aus
    // In diesem Fall hat die Node keine Funktionalitäten,
    // es wird nur der Thread blockiert
    rclcpp::spin(node);

    // Beende rclcpp
    rclcpp::shutdown();
    return 0;
}
    \end{lstlisting}
	Außerdem ist es möglich, eine eigene Klasse von der Node Klasse Erben zu Lassen,\cite{Concise}
	zu sehen in Beispiel \ref{lst:Vererbung}.
	\begin{lstlisting}[language=c++, caption={Erben von der Node Klasse (adaptiert aus \cite{Concise})}, label={lst:Vererbung}]
// Eigene Klasse : Erbt von Node
class LoggerNode : public rclcpp::Node
{
public:
    SimpleNode()
    //    Node Konstruktor
        : Node("simple_node")
    {}
};

int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);

    auto node = std::make_shared<LoggerNode>();
    rclcpp::spin(node);

    rclcpp::shutdown();
    return 0;
}
    \end{lstlisting}

	Es gibt zwei Möglichkeiten zu steuern, wann eine Node ausgeführt wird.

	Zum einem gibt es einen simplen iterativen Ansatz,
	wie er in Beispiel \ref{lst:Iterativ} zu sehen ist.
	In diesem wird die Node in einer spezifischen Frequenz ausgeführt.~\cite{Concise}
	Dadurch hat man schon in Voraus Kontrolle darüber, wie viele Ressourcen eine Node braucht,~\cite{Concise}
	und in welcher Frequenz der Output einer Node,
	etwa in Form von Nachrichten oder Anweisungen an die Hardware, stattfindet.~\cite{Concise}
	\begin{lstlisting}[language=c++, caption={Eine Node, die in einer Spezifischen Frequenz ausgeführt wird \cite{Concise}}, label={lst:Iterativ}]
using namespace std::chrono_literals;

class LoggerNode : public rclcpp::Node
{
public:
    LoggerNode()
        : Node("logger_node")
    {
        counter_ = 0;
        // Erstelle einen Timer
        timer_ = create_wall_timer(
        //           mit einer spzifischen Frequenz
                     500ms,
        //           in der ein Callback aufgerufen wird
                     std::bind(&LoggerNode::timer_callback, this));
    }

    void timer_callback()
    {
        // Erhöhe den Counter und logge ihn in die Konsole
        RCLCPP_INFO(get_logger(), "Hello %d", counter_++);
    }

private:
    rclcpp::TimerBase::SharedPtr timer_;
    int counter_;
};
    \end{lstlisting}

	Der zweite Ansatz, zu sehen in Beispiel \ref{lst:Event},
	ist ein Event-orientierter, in der die Nodes auf spezifische Events,
	etwa ein Sensorsignal oder das Empfangen einer Nachricht, reagiert.~\cite{Concise}
	Dies ist vom Vorteil für Nodes, die nur in diesen Fällen agieren,
	allerdings kann man schlecht die benötigten Ressourcen und den Output Flow im Voraus abschätzen,
	da diese von der Frequenz, in der das Event eintritt, abhängt.~\cite{Concise}
	\begin{lstlisting}[language=c++, caption={Eine Node, die bei Erhalt einer Nachricht ausgeführt wird \cite{Concise}}, label={lst:Event}]
using std::placeholders::_1;

class SubscriberNode : public rclcpp::Node
{
public:
  SubscriberNode()
  : Node("subscriber_node")
  {
    // Höre auf nachrichten
    subscriber_ = create_subscription<std_msgs::msg::Int32>(
      "int_topic", 10,
    // Führe das Callback bei erhalt einer nachricht aus
      std::bind(&SubscriberNode::callback, this, _1));
  }

  void callback(const std_msgs::msg::Int32::SharedPtr msg)
  {
    // Logge die nachricht
    RCLCPP_INFO(get_logger(), "Hello %d", msg->data);
  }

private:
  rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr subscriber_;
};
    \end{lstlisting}
	Das Beispiel nutzt Kommunikation durch Subscription. Dies wird im Folgendem genauer erklärt.

	\subsection{Kommunikation}

	Die Unterteilung eines Computation Graphs in mehrere Nodes erfolgt,
	da er ein Verteiltes System ist.~\cite{Concise}
	Einzelne Nodes können auf unterschiedlichen Maschinen liegen,~\cite{Concise}
	zum Beispiel bei Schwarm-Robotern.
	Selbst wenn alle Nodes, die das Verhalten des Roboters steuern, auf diesem laufen,
	gibt es immer noch Monitoring Tools, die auf dem Gerät des Entwicklers liegen.~\cite{Concise}
	Deswegen müssen einzelne Nodes natürlich auch Prozess- und Maschinen-übergreifend
	miteinander kommunizieren können.
	Die Kommunikation wird, wie oben erwähnt, durch das RMW geregelt.~\cite{Concise}
	Dabei haben die Nodes die Möglichkeit viele verschiedene Nachrichtentypen zu senden,
	die schon durch die Standard Bibliothek verfügbar sind,~\cite{Concise, Concepts}
	allerdings hat man auch die Möglichkeit, eigene zu definieren.~\cite{Concise}
	Es ist auch möglich, Nachrichten zu definieren, die mehrere Daten beinhalten,~\cite{Concepts}
	zum Beispiel eine Nachricht, die sowohl einen \lstinline{int},
	als auch einen \lstinline{string} übermittelt.
	Nachrichten werden in \lstinline{.msg} Dateien
	wie in Beispiel \ref{lst:Msg} definiert.~\cite{Concepts}
	\begin{lstlisting}[caption={Eine Message Definition \cite{Concepts}}, label={lst:Msg}]
# bsp1.msg
int32 my_int
string my_string
    \end{lstlisting}
	Wie in Beispiel \ref{lst:MsgNest} zu sehen können auch andere
	Nachrichten Definitionen eingebunden werden,~\cite{Concepts}
	um so Komplexe Nachrichten zusammen zu stellen.
	\begin{lstlisting}[caption={Eine verschachtelte Message Definition}, label={lst:MsgNest}]
# bsp2.msg
bool my_bool
bsp2 nested
    \end{lstlisting}
	Durch diese einheitliche Definition können auch Nodes, die in unterschiedlichen Sprachen mit
	unterschiedlichen Client Libraries geschrieben wurden miteinander kommunizieren.~\cite{Concepts}

	Es gibt in ROS drei grundlegende Paradigmen zur Kommunikation.
	Allerdings nutzt eine einzelne Node oft mehrere dieser Paradigmen gleichzeitig.~\cite{Concepts}
	Zum Beispiel kann eine Node, die sich um Navigation kümmert, durch eine Action mitgeteilt bekommen,
	wohin sich der Roboter bewegen soll, und durch ein Topic einer Hardware Node mitteilen,
	mit welcher Geschwindigkeit sich der Roboter bewegen soll, und über ein weiteres,
	wohin er lenken soll.

	Im Folgendem werden die Paradigmen erläutert.

	\subsubsection*{Publication/Subscription}

	Diese Kommunikationsart, die auch als Topics bekannt ist, sollte für kontinuierliche Datenströme,
	wie Sensordaten, genutzt werden,~\cite{Concepts} und ist die vermutlich häufigste Kommunikationsart.
	Es handelt sich dabei um eine Asynchrone N zu N Kommunikation,~\cite{Concise}
	das heisst, dass eine beliebige Anzahl an Nodes Daten senden kann,
	und eine beliebige Anzahl diese Empfangen kann,
	ohne dass Sender und Empfänger aufeinander warten müssen.

	Die Sender \textit{publishen} dabei ihre Daten auf eine Art Channel,
	genannt \textit{topic}.~\cite{Concise}
	Das Beispiel \ref{lst:Publisher} zeigt, wie das im Code aussehen kann.
	\begin{lstlisting}[language=c++, caption={Eine Node, die Daten publisht \cite{Concise}}, label={lst:Publisher}]
using namespace std::chrono_literals;
using std::placeholders::_1;

class PublisherNode : public rclcpp::Node
{
public:
  PublisherNode()
  : Node("publisher_node")
  {
    //  Erstelle einen Publisher   der Nachrichten eines bestimmten Typs
    publisher_ = create_publisher<std_msgs::msg::Int32>(
    //                  auf ein Bestimmtes topic publisht.
                        "int_topic",
    //                  QOS Einstellungen
                        10);
    timer_ = create_wall_timer(
      500ms, std::bind(&PublisherNode::timer_callback, this));
  }

  void timer_callback()
  {
    message_.data += 1;
    // Publishe eine Nachricht
    publisher_->publish(message_);
  }

private:
  rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr publisher_;
  rclcpp::TimerBase::SharedPtr timer_;
  std_msgs::msg::Int32 message_;
};
    \end{lstlisting}
	Die Empfänger \textit{subscriben} dieses Topic um die Nachrichten zu erhalten.~\cite{Concise}
	Die Node in Beispiel \ref{lst:Event} tut dies bereits.
	Das Beispiel \ref{lst:Subscriber} ist das selbe, nur erklären die Kommentare dieses mal die
	Kommunikation.
	\begin{lstlisting}[language=c++, caption={Eine Node, die einem Topic subscribt \cite{Concise}}, label={lst:Subscriber}]
using std::placeholders::_1;

class SubscriberNode : public rclcpp::Node
{
public:
  SubscriberNode()
  : Node("subscriber_node")
  {
    // Subscribe ein topic            mit Nachrichten eines bestimmten Typs
    subscriber_ = create_subscription<std_msgs::msg::Int32>(
      // Mit dem Namen
      "int_topic",
      // QoS Einstellungen
      10,
      // Das Callback, das bei erhalt einer Nachricht aufgerufen wird
      std::bind(&SubscriberNode::callback, this, _1));
  }

  void callback(const std_msgs::msg::Int32::SharedPtr msg)
  {
    // Logge die Nachricht
    RCLCPP_INFO(get_logger(), "Hello %d", msg->data);
  }

private:
  rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr subscriber_;
};
    \end{lstlisting}
	Sobald ein Sender Daten sendet, werden diese allen Subscribern zugestellt.~\cite{Concepts}
	Die Empfänger wissen dabei grundsätzlich nicht, von wem die Daten kommen,~\cite{Concepts}
	können dies jedoch herausfinden.~\cite{Concepts}
	Dadurch können Publisher und Subscriber leicht ausgetauscht werden,
	ohne den Rest des Systems zu beeinflussen.~\cite{Concepts}
	Bei beiden können noch Quality of Service (QoS) Einstellungen getroffen werden,
	allerdings sind diese von Wichtigkeit bei Echtzeit Anwendungen und somit nicht Thema dieser Arbeit.

	\subsubsection*{Service}

	Ein Service ist ein Remote Procedure Call.~\cite{Concepts}
	Eine Node sendet diesen Call an eine andere Node,
	die ihn ausführt und das Ergebnis zurück sendet.~\cite{Concise}
	Dabei wartet die erste Node auf das Ergebnis.~\cite{Concise}

	Ein Service besteht aus zwei Komponenten:
	dem Service Server, der den Service ausführt,~\cite{Concepts}
	und einem Service Client, der diesen anfordert.~\cite{Concepts}
	Ein Service wird in einer \lstinline{.srv} Datei definiert,
	bevor ein Server diesen implementieren kann.~\cite{Concepts}
	\begin{lstlisting}[caption={Eine Service Definition \cite{Concepts}}, label={lst:Srv}]
# Die Request Nachricht
uint32 a
uint32 b
---
# Die Antwort Nachricht
uint32 sum
    \end{lstlisting}
	Wie im Beispiel \ref{lst:Srv} zu sehen ist, besteht sie aus zwei Nachrichtendefinitionen,
	getrennt durch ''\lstinline{---}''.
	Alle Regeln, die für Nachrichtendefinitionen gelten, gelten für Service Definitionen.

	Zu dieser Definition kann ein Server den Service
	implementieren und unter einem bestimmten Namen anbieten.~\cite{Tutorials}
	Wie im Beispiel \ref{lst:Server} zu sehen ist, werden dafür Callbacks genutzt.
	\begin{lstlisting}[language=c++, caption={Eine Node, die einen Service anbietet (Adaptiert aus \cite{Concepts})}, label={lst:Server}]
// Das Callback
void add(
//       das eine Request Nachricht erhält
         const std::shared_ptr<example_interfaces::srv::AddTwoInts::Request> request,
//                 und einen Pointer, unter dem es eine Antwort ablegen kann
         std::shared_ptr<example_interfaces::srv::AddTwoInts::Response> response)
{
    response->sum = request->a + request->b;
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    auto node = rclcpp::Node::make_shared("add_two_ints_server");

    auto service =
    //  Erstelle einen Service     mit dieser Definition
        node->create_service<example_interfaces::srv::AddTwoInts>(
    //  Unter diesem Namen
        "add_two_ints",
    //  Der dieses Callback nutzt.
        &add);

    rclcpp::spin(node);
    rclcpp::shutdown();
}
    \end{lstlisting}
	Diesen Namen kann dann der Client nutzen, um den Service anzufordern.~\cite{Tutorials}
	Dabei muss er, wie im Beispiel \ref{lst:Client} zu sehen, muss er dabei auf eine Antwort warten.
	\begin{lstlisting}[language=c++, caption={Eine Node, die einen Service nutzt (Adaptiert aus \cite{Concepts})}, label={lst:Client}]
using namespace std::chrono_literals;

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    auto node = rclcpp::Node::make_shared("add_two_ints_client");
    auto client =
    //  Erstelle einen Client,  Der einen Service mit diesem Typ nutzt
        node->create_client<example_interfaces::srv::AddTwoInts>(
    //          der unter diesem Namen erreichbar ist.
                "add_two_ints");

    // Erstelle eine Request
    auto request = std::make_shared<example_interfaces::srv::AddTwoInts::Request>();
    // Setze die Werte der Request Nachricht
    request->a = 1;
    request->b = 2;

    // Warte, bis der Service verfügbar ist
    client->wait_for_service();

    // Sende die Request
    auto result = client->async_send_request(request);
    // Warte bis eine Antwort kommt (oder die Request fehlschlägt)
    if (rclcpp::spin_until_future_complete(node, result) ==
            rclcpp::FutureReturnCode::SUCCESS)
    {
        // Logge die Antwort
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Sum: %ld", result.get()->sum);
    } else {
        // Logge den Fehler
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Failed to call service");
    }

    rclcpp::shutdown();
    return 0;
}
    \end{lstlisting}
	Ein Service darf nur von einem Server unter dem selben Namen angeboten werden,~\cite{Concepts}
	Was passiert, wenn er von mehreren angeboten wird, ist undefiniert.~\cite{Concepts}
	Allerdings ist es möglich, dass mehrere Clients denselben Service nutzen.~\cite{Concepts}

	Es ist jedoch zu beachten, dass der Client wartet, biss der Service erfüllt ist. ~\cite{Concise}
	Das bedeutet, wenn der Server bei der Erfüllung des Services lange Berechnungszeiten hat,
	wird der Client entsprechend blockiert.~\cite{Concise}
	Daher werden von Services in der Regel schnelle Berechnungszeiten erwartet.~\cite{Concepts}
	Für länger laufende Prozesse sollten Actions genutzt werden.~\cite{Concepts}

	\subsubsection*{Actions}

	Actions sind eine Art lang andauernder Remote Procedure Call
	mit regelmäßigen Feedback und der Möglichkeit,
	ihn abzubrechen oder vorzeitig zu beenden.~\cite{Concepts}
	Im gegensatz zu Services sind Actions asynchron.~\cite{Concise}

	Ähnlich wie bei Services gibt es auch hier einen Server und ein bis mehrere Clients.~\cite{Concepts}
	Außerdem wird auch wieder die Action in einer eigenen
	\lstinline{.action} Datei definiert.~\cite{Tutorials}
	Weiterhin bietet auch hier bietet der Server die Action unter
	einem bestimmten Namen an,~\cite{Concepts} wie es bei den Services der Fall ist.
	Da hier mehr Callbacks genutzt werden als bei den Services, entfällt hier das Beispiel,
	um nicht den Rahmen zu sprengen.
	Diesen Namen können dann wiederum Clients nutzen, um die Action anzufordern.

	Actions haben Vergleich zu Services einen gewissen Overhead,~\cite{Concepts}
	weshalb sie für länger laufende Prozeduren wie etwa Anweisungen
	an das Navigations-System gedacht sind.~\cite{Concepts}
	Für schnelle Calls sollte man Services nutzen.~\cite{Concepts}

	\section{Workspace}

	In vielen Sprachen werden Projekte in einem Ordner organisiert.
	Ähnlich ist es bei ROS. Hier nennt sich dieser Ordner Workspace.~\cite{Concise}
	In diesem befindet sich der vom Entwickler geschriebene Code, organisiert in Packages.~\cite{Concise}
	Weiterhin werden die kompilierten Packages
	in einem speziellen Unterordner installiert,~\cite{Concise}
	analog zu dem aus anderen Projekten bekannten Output Ordner.
	Zusätzlich zu dem Computation Graph kann ein Workspace auch Build-Systeme
	und Tools zum Starten von Nodes beinhalten.~\cite{Concise}
	Speziell ist dies der Fall für die standard ROS Installation,
	die auch in einem Workspace organisiert ist.

	Wie oben schon erwähnt, ist Code innerhalb eines Workspaces in Packages organisiert.
	Diese bilden die Grundbausteine eines Workspaces.~\cite{Concise}
	Packages sind hierbei ähnlich zu Packages wie man sie zum Beispiel aus dem Package Manager einer
	Linux distribution kennt: Sie können Executables enthalten~\cite{Concise},
	die Nodes und deren Funktionalitäten ausführen oder aber Libraries~\cite{Concise},
	die wiederum Funktionalitäten für andere Packages bereitstellen.
	Allerdings kann ein ROS Package noch etwas weiters beinhalten,
	die sich nur bedingt mit Software Packages vergleichen lässt,
	die Message, Service und Action Definitionen.~\cite{Concise}
	Ein Package, das eine Library oder eine Message Definition eines anderen Packages nutzt,
	ist von diesem Abhängig.
	Man spricht hierbei von einer Dependency.

	Um die in einem Workspace enthaltene Software nutzen zu können,
	muss er erst aktiviert werden.~\cite{Concise}
	Dazu gibt für gewöhnlich in dem install Ordner eine setup Datei, die geladen werden muss.
	In der Bash shell geschieht das durch den Befehl
	\lstinline[language=bash]{source ./install/setup.bash}.
	Leicht anders sieht es mit der Aktivierung der ROS 2 Basis-Installation aus.
	Dies geschieht durch den Befehl
	\lstinline[language=bash]{source /opt/ros/<distro>/setup.bash}.~\cite{Concise}

	Es ist möglich und üblich mehrere Workspaces gleichzeitig aktiviert zu haben.
	Dabei nennt man den ersten aktivierten Workspace das Underlay
	und danach aktivierte Workspaces Overlays.~\cite{Concise}
	Dabei gelten zwei Regeln:
	\begin{itemize}
		\item Besitzt ein Package im Overlay Dependencies,
		      die nicht durch andere Packages im Overlay erfüllt werden,
		      so müssen diese durch das Underlay erfüllt werden.~\cite{Concise}
		\item Enthalten sowohl das Overlay, als auch das Underlay Packages mit dem selben Namen,
		      so überschattet das Package aus dem Overlay das Package aus dem Underlay.~\cite{Concise}
	\end{itemize}
	Von vielen ROS Applikationen wird das selbe Underlay genutzt: Die Standard ROS Applikation
	Dies ist der Fall, da dieser Workspace die Client Bibliotheken, Tools zum Starten von Nodes,
	zum Monitoring und vielem mehr bereit stellt.

	\section{Zusammenfassung}

	Die vorangegangenen Kapitel haben detailliert aufgezeigt,
	wie ROS 2 als Entwicklungsplattform genutzt werden kann.
	Dabei wurde der Fokus auf die Organisation eines Projekts im Workspace
	sowie den Aufbau einer ROS-Applikation im Computation Graph gelegt.
	Die Strukturierung in Nodes, die spezifische Aufgaben übernehmen
	und miteinander kommunizieren, ermöglicht eine modulare und skalierbare Entwicklung.

	Ein zentraler Aspekt, der ROS 2 auszeichnet, ist die Art und Weise der Kommunikation.
	Die Verwendung von Topics als asynchroner Austausch
	ermöglicht es einer beliebigen Anzahl von Nodes, Daten zu publishen,
	während andere Nodes diese Daten subscriben können.
	Des Weiteren bieten Services die Möglichkeit für RPC-Anfragen,
	bei denen eine Node eine Anfrage sendet und auf deren Ausführung wartet.
	Actions hingegen erweitern dieses Konzept durch asynchrone RPC-Anfragen,
	bei denen die anfragende Node regelmäßige Rückmeldungen erhält
	und eine Nachricht bei Abschluss der Anfrage.

	Wichtig ist zu betonen, dass die Eignung von ROS für die Steuerung von Fahrzeugen
	und die Realisierung autonomer Fahrfunktionen stark von den Echtzeitcharakteristika abhängt.
	Dieser Aspekt bildet jedoch nicht den Kern dieser Arbeit,
	sondern wird in einer separaten Arbeit ausführlich behandelt.

	Insgesamt bietet ROS 2 mit seinen vielfältigen Kommunikationsmechanismen und der modularen
	Struktur eine Grundlage für die Entwicklung von komplexen, verteilten Systemen.
	Die erarbeiteten Erkenntnisse legen den Grundstein für die praktische Anwendung von ROS 2
	in unterschiedlichen Kontexten, wobei die Anpassung an spezifische Anforderungen und
	Echtzeitbedingungen weitere Vertiefung.

\end{samepage}

\printbibliography

\end{document}
