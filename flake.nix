{
  description = "A flake to create the latex environment needed for my paper";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = (import nixpkgs) {
          inherit system;
        };
        tex = pkgs.texlive.combined.scheme-full;
      in
      {
        packages = {
          arbeit = pkgs.stdenvNoCC.mkDerivation rec {
            name = "seminararbeit";
            src = self;
            buildInputs = [ pkgs.coreutils tex ];
            phases = [ "unpackPhase" "buildPhase" "installPhase" ];
            buildPhase = ''
              export PATH="${pkgs.lib.makeBinPath buildInputs}";
              mkdir -p .cache/texmf-var
              env TEXMFHOME=.cache TEXMFVAR=.cache/texmf-var \
                latexmk -interaction=nonstopmode -pdf -lualatex \
                arbeit.tex
            '';
            installPhase = ''
              mkdir -p $out
              cp .cache/arbeit.pdf $out/
            '';
          };
          presentation = pkgs.stdenvNoCC.mkDerivation rec {
            name = "präsentation";
            src = self;
            buildInputs = [ pkgs.coreutils tex ];
            phases = [ "unpackPhase" "buildPhase" "installPhase" ];
            buildPhase = ''
              export PATH="${pkgs.lib.makeBinPath buildInputs}";
              mkdir -p .cache/texmf-var
              env TEXMFHOME=.cache TEXMFVAR=.cache/texmf-var \
                latexmk -interaction=nonstopmode -pdf -lualatex \
                präsentation.tex
            '';
            installPhase = ''
              mkdir -p $out
              cp .cache/präsentation.pdf $out/
            '';
          };
        };
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.coreutils
            tex
            (pkgs.writeShellScriptBin "buildArbeit" ''latexmk -pdf -lualatex arbeit.tex'')
            (pkgs.writeShellScriptBin "buildPräsentation" ''latexmk -pdf -lualatex präsentation.tex'')
          ];
          shellHook = ''
            mkdir -p .cache/texmf-var
          '';
          TEXMFHOME = ".cache";
          TEXMFVAR = ".cache/texmf-var";
        };
      }
    );
}
